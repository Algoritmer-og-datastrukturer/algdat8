import java.io.*;
import java.nio.file.Files;


public class LempelZiv {
    public static final int DEFAULT_BUFF_SIZE = 12;
    private final int bufferStr;
    public Reader txtIn;
    public PrintWriter txtUt;
    public StringBuffer sokBuffer;


    public LempelZiv() {
        this.bufferStr = DEFAULT_BUFF_SIZE;
        this.sokBuffer = new StringBuffer(bufferStr);
    }

    private void kalibrerBuffer() {
        if (sokBuffer.length() > bufferStr) {
            sokBuffer.delete(0, sokBuffer.length() - bufferStr);
        }
    }


    public void uKomprimer(String infile) throws IOException {
        txtIn = new BufferedReader(new FileReader(infile));
        txtUt = new PrintWriter(new BufferedWriter(new FileWriter(infile+".Ukomp")));
        StreamTokenizer token = new StreamTokenizer(txtIn);

        token.ordinaryChar(' ');
        token.ordinaryChar('.');
        token.ordinaryChar('-');
        token.ordinaryChar('\n');
        token.wordChars('\n', '\n');
        token.wordChars(' ', '}');

        int offset/**Sskrt**/, lengde;
        while (token.nextToken() != StreamTokenizer.TT_EOF) {
            switch (token.ttype) {
                case StreamTokenizer.TT_WORD:
                    sokBuffer.append(token.sval);

                    txtUt.print(token.sval);
                    kalibrerBuffer();
                    break;

                case StreamTokenizer.TT_NUMBER:
                    offset = (int)token.nval;
                    token.nextToken();
                    if (token.ttype == StreamTokenizer.TT_WORD) {
                        sokBuffer.append(offset).append(token.sval);

                        txtUt.print(offset+token.sval);
                        break;
                    }

                    token.nextToken();
                    lengde = (int)token.nval;
                    String output = sokBuffer.substring(offset, offset+lengde);

                    txtUt.print(output);
                    sokBuffer.append(output);
                    kalibrerBuffer();
                    break;
                default:
                    // tom
            }
        }
        txtIn.close();
        txtUt.flush();
        txtUt.close();
    }


    public void komprimer(String infile) throws IOException {
        txtIn = new BufferedReader(new FileReader(infile));
        txtUt = new PrintWriter(new BufferedWriter(new FileWriter(infile+".komp")));

        int nesteBokst;
        int tempIndex;
        String funnetMatch = "  ";
        int matchIndex = 0;


        while ((nesteBokst = txtIn.read()) != -1) {
            tempIndex = sokBuffer.indexOf(funnetMatch + (char)nesteBokst);

            if (tempIndex != -1) {
                funnetMatch += (char)nesteBokst;
                matchIndex = tempIndex;
            } else {
                
                String kryptertString =
                        "~"+matchIndex+"~"+funnetMatch.length()+"~"+(char)nesteBokst;
                String concat = funnetMatch + (char)nesteBokst;
                // hvis stringen funnet er mindre en teksten bytter vi ut
                if (kryptertString.length() <= concat.length()) {
                    txtUt.print(kryptertString);
                    sokBuffer.append(concat);
                    funnetMatch = "";
                    matchIndex = 0;
                } else {
                    funnetMatch = concat; matchIndex = -1;
                    while (funnetMatch.length() > 1 && matchIndex == -1) {
                        txtUt.print(funnetMatch.charAt(0));
                        sokBuffer.append(funnetMatch.charAt(0));
                        funnetMatch = funnetMatch.substring(1, funnetMatch.length());
                        matchIndex = sokBuffer.indexOf(funnetMatch);
                    }
                }
                
                kalibrerBuffer();
            }
        }
        // når enden av fil, skriver ut
        if (matchIndex != -1) {
            String codedString =
                    "~"+matchIndex+"~"+funnetMatch.length();
            if (codedString.length() <= funnetMatch.length()) {
                txtUt.print("~"+matchIndex+"~"+funnetMatch.length());
            } else {
                txtUt.print(funnetMatch);
            }
        }
        txtIn.close();
        txtUt.flush();
        txtUt.close();
    }

    public static void main(String [] args) throws IOException {



        LempelZiv lz = new LempelZiv();


        //lz.komprimer("C:\\Users\\Jolse\\no\\AlgDat\\OvingAlgdat8\\src\\Resources\\diverse.txt");

        lz.uKomprimer("C:\\Users\\Jolse\\no\\AlgDat\\OvingAlgdat8\\src\\Resources\\huffmanDecode.txt");

    }
}



